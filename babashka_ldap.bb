(ns babashka-ldap)

(require '[clojure.string :as str])
(require '[babashka-utility :as util])
(require '[babashka-config-parser :as conf])

(defn parse-AD-obj
  [ad-obj]
  (map str/trim (str/split ad-obj #"\s+:")))

(defn parse-AD-objs
  [ad-objs]
  (map parse-AD-obj ad-objs))

(defn filter-empty-nodes
  "Filter out empty nodes from an AD results tree"
  [tree]
  (remove #(every? str/blank?
                   (flatten %))
          tree))

(defn assemble-multiline-entries
  "Repair inconsistent line-breaks to form full AD entries"
  [entries]
  (reduce  #(if (re-find #"\s+:" %2)
              (concat %1 (list %2))
              (concat (drop-last %1) (list (str (last %1) (str/trim %2))))) ;; Replace last entry with a concat of the last entry and processed entry
           (list) entries))

(defn pre-process-tree
  "Prepare the tree to be turned into an AD object"
  [tree]
  (let [split-obj-entries (map #(util/non-destructive-split % #":.*" true) tree)]
    ;(doall (map println (second split-obj-entries)))
    (map assemble-multiline-entries split-obj-entries)))

;; processed-tree legit just a seq of seq of seqs that contain the entries of the AD-object. Each entry still needs to be split into key/value

(defn string-to-tree
  "AD string to tree"
  [str-data]
  (conf/generate-block-tree nil ["\n\n"] str-data))

(defn parse-AD-results-tree
  "Parse the results of a search"
  [tree]
  (let [processed-tree (pre-process-tree tree)
        parsed-tree-unfiltered (map parse-AD-objs processed-tree)
        parsed-tree (filter-empty-nodes parsed-tree-unfiltered)]
    ;;(println (map #(list (count %) %)(first parsed-tree)))
    ;;(doall (map println (first parsed-tree)))
    ;;(doall (map #(println "=-=-=-" %) (second processed-tree)))
    ;;(doall processed-tree)
    (map #(apply hash-map (apply concat %)) parsed-tree))) ;; (apply concat %) works as a single-level flatten so that we don't flatten dictionaries

(defn parse-AD-string
  "Convenience function that creates and parses a tree"
  [str-data]
  (-> str-data
      string-to-tree
      parse-AD-results-tree))

(defn get-AD-key
  [ad-obj]
  (first (parse-AD-obj ad-obj)))

(defn get-AD-value
  [ad-obj]
  (second (parse-AD-obj)))

(defn get-tree-attributes
  [tree attribute]
  (map #(get % attribute) tree))

(defn get-usernames
  [tree]
  (let [emails (get-tree-attributes tree "EmailAddress")
        usernames (map #(first (str/split % #"@")) emails)]
    usernames))

(defn parse-AD-file
  [file-name]
  (parse-AD-results-tree (string-to-tree (slurp file-name))))

;; tree -> seq of multi-line string

(defn -main
  [& args]
  (let [;;parsed-args (parse-args args)
        tree (string-to-tree (slurp "AD-Users-2022-05-24.txt")) 
        parsed-tree (parse-AD-results-tree tree)]
    ;;(util/non-destructive-split % #"")
                                        ;;(println "MAIN:" parsed-tree)
    
    ;;(println "MAIN:" (map println (second (map #(non-destructive-split % #":.*" true) tree))))
    ;;(println "MAIN:" (map println (map #(non-destructive-split % #":.*" true) (list (nth tree 3)))))
    ;;(println (get-usernames parsed-tree))
))
(-main)
